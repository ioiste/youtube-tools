# YouTube Tools

JavaScript tool to better interact with your data in your YouTube account. Currently implemented:
* Login/Authorize the application via OAuth2 to user account
* Revoke access to user account
* Get subscribed channels
* Display subscribed channels with subscription date
* Check channels which haven't uploaded for 2 years, 1 year or half a year
* Unsubscribe to the channels meeting filtering criteria
* Rate a video (like/dislike/none)
* Rate multiple videos (like/dislike/none) 
  * from a playlist
  * from a Spotify YourLibrary.json data dump [requested from them ("Download your data")](https://www.spotify.com/account/privacy/).

![Demo gif](https://i.imgur.com/oW8IfwB.gif "Demo gif")

![Figure 1](https://i.imgur.com/CnUMgEz.png "Figure 1")

###### Authorization
Make sure you've got your [application's credentials from Google's Developer Console](https://console.developers.google.com/apis/credentials) at hand.

You need your API Key and Client ID:
- The API Key must be allowed to access the YouTube Data API v3.
- The OAuth 2.0 Client must be configured to have "Authorized JavaScript origins" and "Authorized redirect URIs" set as the URL where the web app is running (like `https://youtube-tools.oiste.ro`). 

**The API Key and Client ID are stored only on your browser. They're not transmitted to any backend (besides Google's YouTube API) and are solely used to make authenticated API requests.**

###### Would be nice (TO DO):
* If YouTube allowed access to watch history, it would be interesting to see some data like total watch time
* If YouTube allowed access to the watch later playlist, I would create a better way to interact with it to remove old/uninteresting videos
* For music, filter the "Your Likes" playlist based on genre and song length
* For music, get all songs as a list, give it to an AI model to compose playlists based on user input (style/occasion/context) and then use the output to create a new playlist in YouTube
