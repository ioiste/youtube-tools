"use strict";
let subscriptions = [];
let channelsMeetingFilterCriteria = [];

function getSubscriptionsPage(pageToken) {
    return gapi.client.youtube.subscriptions.list({
        "part": ["snippet"],
        "maxResults": 50,
        "mine": true,
        "pageToken": pageToken !== "" ? pageToken : ""
    })
        .then(function (response) {
                return response.result;
            },
            function (err) {
                handleYoutubeAPIErrorResponse(err);
            });
}

async function getSubscriptions() {
    if (GoogleAuth.currentUser.get().getBasicProfile() === undefined) {
        createToastNotification('Failed', 'It looks like you\'re not logged in.');
        return;
    }

    document.getElementById('get-subscriptions-button-spinner').style.display = 'inline-block';
    document.getElementById('get-subscriptions-button').disabled = true;

    let userId = GoogleAuth.currentUser.get().getBasicProfile().getId();

    let pageToken = undefined;
    subscriptions = [];
    if (
        localStorage.getItem(userId + '-subscriptions') === null
        || !shouldUseCacheForSubscriptionsListRetrieval()
    ) {
        do {
            let subscriptionsResult = await getSubscriptionsPage(pageToken);
            pageToken = subscriptionsResult.nextPageToken;
            subscriptions.push(...subscriptionsResult.items);
        } while (pageToken !== undefined);
        subscriptions = [...new Set(subscriptions.map(JSON.stringify))].map(JSON.parse);
        localStorage.setItem(userId + '-subscriptions', JSON.stringify(subscriptions.map(function (subscription) {
            return JSON.stringify(subscription);
        })));
        localStorage.removeItem(userId + '-channelsDataArray');

        if (document.getElementById('subscriptions-list').classList.contains('show')) {
            document.getElementById('subscriptions-list').classList.remove('show');
            document.getElementById('subscriptions-list').style.display = 'none';
            document.getElementById('subscriptions-list').textContent = '';
            document.getElementById('show-subscriptions-button').innerText = 'Show subscriptions (scroll down)';
        }
    } else {
        let subscriptionsJsonArray = JSON.parse(localStorage.getItem(userId + '-subscriptions'));
        subscriptions = subscriptionsJsonArray.map(function (subscriptionJson) {
            return JSON.parse(subscriptionJson);
        });
    }
    document.getElementById('get-subscriptions-button-spinner').style.display = 'none';
    document.getElementById('get-subscriptions-button').disabled = false;

    createToastNotification('Success', 'Fetched ' + subscriptions.length + ' subscriptions.');
}

function showSubscriptions() {
    if (subscriptions.length === 0) {
        createToastNotification('Info', 'No subscriptions to display. Get them first.');
        return;
    }

    if (document.getElementById('subscriptions-list').classList.contains('show')) {
        document.getElementById('subscriptions-list').classList.remove('show');
        document.getElementById('subscriptions-list').style.display = 'none';
        document.getElementById('show-subscriptions-button').innerText = 'Show subscriptions (scroll down)';
        return;
    }

    document.getElementById('subscriptions-list').textContent = '';
    for (let subscriptionKey in subscriptions) {
        if (subscriptions.hasOwnProperty(subscriptionKey)) {
            const sub = subscriptions[subscriptionKey];
            displaySubscriptionBox(
                sub.snippet.title,
                sub.snippet.resourceId.channelId,
                sub.snippet.thumbnails.default.url,
                sub.snippet.publishedAt
            );
        }
    }
    document.getElementById('subscriptions-list').classList.add('show');
    document.getElementById('subscriptions-list').style.display = 'flex';
    document.getElementById('show-subscriptions-button').innerText = 'Hide subscriptions';
}

function displaySubscriptionBox(title, channelId, image, subscribeDate) {
    subscribeDate = new Date(subscribeDate).toISOString().split('T')[0];

    let wrapperDiv = document.createElement('div');
    wrapperDiv.className = 'col-3 p-0';

    let row = document.createElement('div');
    row.className = 'row'

    let imgColumn = document.createElement('div');
    imgColumn.className = 'col-3'

    let img = document.createElement('img');
    img.src = image;
    img.alt = title;

    imgColumn.appendChild(img);
    row.appendChild(imgColumn);

    let titleColumn = document.createElement('div');
    titleColumn.className = 'col mt-4 ml-3'

    let h5 = document.createElement('h5');
    h5.innerText = title;
    let channelLink = document.createElement('a');
    channelLink.href = 'https://www.youtube.com/channel/' + channelId;
    channelLink.appendChild(h5);

    let subSince = document.createElement('small');
    subSince.innerText = 'Subscribed since ' + subscribeDate;

    titleColumn.appendChild(channelLink);
    titleColumn.appendChild(subSince);
    row.appendChild(titleColumn);
    wrapperDiv.appendChild(row);

    document.getElementById('subscriptions-list').appendChild(wrapperDiv);
}

function createToastNotification(title, body, delay = 10000, autohide = true) {
    let notificationElement = document.getElementById('subscriptions-fetched-toast').cloneNode(true);
    $(notificationElement).children('.toast-header').children('#subscriptions-fetched-toast-title')[0].innerText = title;
    $(notificationElement).children('#subscriptions-fetched-toast-body')[0].innerText = body;

    document.getElementById('notifications-area').appendChild(notificationElement);
    $(notificationElement).toast({delay: delay, autohide: autohide});
    $(notificationElement).toast('show');
}

async function getPlaylistData(playlistId, maxResults = 1, skipCache = false) {
    let items = [];
    let pageToken = null;

    if (!skipCache && localStorage.getItem(playlistId)) {
        return JSON.parse(localStorage.getItem(playlistId));
    }

    do {
        try {
            let response = await gapi.client.youtube.playlistItems.list({
                "part": ["snippet"],
                "maxResults": maxResults,
                "playlistId": playlistId,
                "pageToken": pageToken
            });

            items.push(...response.result.items);

            if (maxResults !== 1) {
                pageToken = response.result.nextPageToken;
            }
        } catch (err) {
            console.error("Execute error", err);
            createToastNotification('Failed', 'Unable to fetch data from Youtube API.');
        }
    } while (pageToken);

    localStorage.setItem(playlistId, JSON.stringify(items));

    return items;
}

async function computeChannelsToUnsubscribe() {
    if (GoogleAuth.currentUser.get().getBasicProfile() === undefined) {
        createToastNotification('Failed', 'It looks like you\'re not logged in.');
        return;
    }

    if (subscriptions.length === 0) {
        createToastNotification('Info', 'No subscribed channels to check. Get them first.');
        return;
    }

    let userId = GoogleAuth.currentUser.get().getBasicProfile().getId();

    channelsMeetingFilterCriteria = [];
    if (!isNaN(parseInt(document.getElementById('unsubFilter').value))) {
        document.getElementById('filter-check-channels-button-spinner').style.display = 'inline-block';
        document.getElementById('filter-check-channels-button').setAttribute('disabled', 'disabled');

        let channelsDataArray = Array.from(Object.keys(subscriptions).map(function (k) {
            return {
                subscriptionId: subscriptions[k].id,
                channelTitle: subscriptions[k].snippet.title,
                channelId: subscriptions[k].snippet.resourceId.channelId,
            };
        }));


        if (localStorage.getItem(userId + '-channelsDataArray') === null) {
            let progressBarElement = document.getElementById('filter-check-channels-progress-bar');
            progressBarElement.parentElement.style.display = 'flex';
            for (const [key, channelsDataArrayElement] of channelsDataArray.entries()) {
                let uploadsPlaylist =
                    'UU' + channelsDataArrayElement.channelId.substr(2, channelsDataArrayElement.channelId.length);
                let channelUploadsPlaylistData = await getPlaylistData(
                    uploadsPlaylist,
                    1,
                    !shouldUseCacheForSubscriptionsListRetrieval()
                );
                if (undefined === channelUploadsPlaylistData) {
                    createToastNotification(
                        'Failed',
                        'Could not get uploads for channel ' + channelsDataArrayElement.channelTitle + '.'
                    )
                    continue;
                }

                channelsDataArray[key].lastUploadDate = channelUploadsPlaylistData[0] !== undefined ?
                    channelUploadsPlaylistData[0].snippet.publishedAt :
                    null;
                let percentage = (key / channelsDataArray.length) * 100;
                progressBarElement.setAttribute('aria-valuenow', percentage.toString());
                progressBarElement.style.width = percentage + '%';
            }
            progressBarElement.parentElement.style.display = 'none';
            localStorage.setItem(userId + '-channelsDataArray', JSON.stringify(channelsDataArray.map(function (channel) {
                return JSON.stringify(channel);
            })));
        } else {
            let channelsDataArrayJson = JSON.parse(localStorage.getItem(userId + '-channelsDataArray'));
            channelsDataArray = channelsDataArrayJson.map(function (channelJson) {
                return JSON.parse(channelJson);
            });
        }

        let filterDate = new Date();
        switch (parseInt(document.getElementById('unsubFilter').value)) {
            case 1:
                filterDate.setMonth(filterDate.getMonth() - 6);
                break;
            case 2:
                filterDate.setFullYear(filterDate.getFullYear() - 1);
                break;
            case 3:
                filterDate.setFullYear(filterDate.getFullYear() - 2);
                break
            default:
                filterDate = null;
                break;
        }

        if (filterDate !== null) {
            for (const channelsDataArrayElement of channelsDataArray) {
                if (
                    channelsDataArrayElement.lastUploadDate === null ||
                    new Date(channelsDataArrayElement.lastUploadDate) < filterDate
                ) {
                    channelsMeetingFilterCriteria.push(channelsDataArrayElement);
                }
            }
        }

        document.getElementById('filter-check-channels-button-spinner').style.display = 'none';
        document.getElementById('filter-check-channels-button').removeAttribute('disabled');

        if (channelsMeetingFilterCriteria.length === 0) {
            createToastNotification('Info', 'No channels found for the selected filter.');
        } else {
            displayChannelsToUnsubscribeConfirmation(channelsMeetingFilterCriteria);
        }
    } else {
        createToastNotification('Info', 'No filter selected.');
    }
}

function displayChannelsToUnsubscribeConfirmation(channelsMeetingFilterCriteria) {
    document.getElementById('confirmUnsubModalLabel').innerText =
        'Channels found for your filter: ' + channelsMeetingFilterCriteria.length

    let channelsUrls = Array.from(Object.keys(channelsMeetingFilterCriteria).map(function (k) {
        return '<a href="https://www.youtube.com/channel/' + channelsMeetingFilterCriteria[k].channelId +
            '" id="' + channelsMeetingFilterCriteria[k].channelId + '">' +
            channelsMeetingFilterCriteria[k].channelTitle +
            '</a> (last upload ' + channelsMeetingFilterCriteria[k].lastUploadDate + ')<br>';
    }));

    document.getElementById('confirmUnsubModalBody').innerHTML =
        '<h5> Channels with last upload older than your filter (or no uploads whatsoever) </h5>' +
        '<div id="channelsToUnsubscribe">' + channelsUrls.join('') + '</div>';
    $('#confirmUnsubModal').modal('show');
}

function deleteSubscription(subscriptionId) {
    return gapi.client.youtube.subscriptions.delete({
        "id": subscriptionId
    })
        .then(function (response) {
                return response.status === 204;
            },
            function (err) {
                handleYoutubeAPIErrorResponse(err);
            });
}

async function unsubscribeChannels() {
    document.getElementById('confirmUnsubModalConfirmButton').disabled = true;
    let progressBarElement = document.getElementById('unsubscribe-channels-progress-bar');
    progressBarElement.parentElement.style.display = 'flex';

    let successCount = 0;
    let failedCount = 0;
    for (const [key, channel] of channelsMeetingFilterCriteria.entries()) {
        let unsubFeedback;
        if (await deleteSubscription(channel.subscriptionId)) {
            unsubFeedback = document.createElement('span').innerHTML = '✅';
            successCount++;
        } else {
            unsubFeedback = document.createElement('span').innerHTML = '❌';
            failedCount++;
        }
        document.getElementById(channel.channelId).after(unsubFeedback);

        let percentage = (key / channelsMeetingFilterCriteria.length) * 100;
        progressBarElement.setAttribute('aria-valuenow', percentage.toString());
        progressBarElement.style.width = percentage + '%';
    }

    let userId = GoogleAuth.currentUser.get().getBasicProfile().getId();
    localStorage.removeItem(userId + '-channelsDataArray');

    document.getElementById('channelsToUnsubscribe').before(
        document.createElement('p').innerHTML =
            'Successfully unsubscribed from ' + successCount + ' channels. ' +
            (failedCount !== 0 ? 'Failed to unsubscribe from ' + failedCount + ' channels.' : '')
    );

    progressBarElement.parentElement.style.display = 'none';
}

async function rateSpotifyTracks(rating) {
    if (!isValidRating(rating)) {
        return;
    }

    let progressBarElement = document.getElementById('rate-spotify-videos-progress-bar');
    progressBarElement.parentElement.style.display = 'flex';

    const spotifyData = document.getElementById('spotify-tracks-input').value;
    const tracks = JSON.parse(spotifyData).tracks;

    for (let key in tracks) {
        let percentage = ((key / tracks.length) * 100);
        progressBarElement.setAttribute('aria-valuenow', percentage.toString());
        progressBarElement.style.width = percentage + '%';

        if (!tracks.hasOwnProperty(key)) {
            continue
        }

        let track = tracks[key];
        let query = track.artist + " " + track.track;
        let videoId;

        if (shouldUseCacheForRateVideos()) {
            videoId = localStorage.getItem(query);
        }

        if (localStorage.getItem(videoId) === 'rated') {
            continue;
        }

        if (!videoId) {
            let searchResults = await searchYoutube(query);

            if (searchResults.items.length > 0) {
                videoId = searchResults.items[0].id.videoId;

                if (shouldUseCacheForRateVideos()) {
                    localStorage.setItem(query, videoId);
                }
            } else {
                createToastNotification('Failed', 'No YouTube video found for track: ' + query, 1, false);
                continue;
            }
        }

        if (localStorage.getItem(videoId) === 'rated') {
            continue;
        }

        await rateVideoById(videoId, rating);
    }

    progressBarElement.parentElement.style.display = 'none';
}

async function searchYoutube(query) {
    return gapi.client.youtube.search.list({
        "part": "snippet",
        "maxResults": 1,
        "q": query,
        "type": "video"
    }).then(function (response) {
        return response.result;
    }, function (err) {
        handleYoutubeAPIErrorResponse(err);
    });
}

function rateVideo(rating) {
    if (!isValidRating(rating)) {
        return;
    }

    const videoUrl = document.getElementById('video-url-input');
    const urlParams = new URLSearchParams(videoUrl.value);
    const videoId = urlParams.get('https://www.youtube.com/watch?v');

    return rateVideoById(videoId, rating);
}

function rateVideoById(videoId, rating) {
    if (!isValidRating(rating) || videoId.length === 0) {
        return;
    }

    return gapi.client.youtube.videos.rate({
        "id": videoId,
        "rating": rating
    }).then(function () {
        createToastNotification('Success', 'Successfully rated a video (' + rating + ').');
        for (let activeButton of document.getElementsByClassName('active')) {
            activeButton.classList.remove('active');
        }

        if (shouldUseCacheForRateVideos()) {
            localStorage.setItem(videoId, 'rated');
        }

        document.getElementById('rate-video-button-' + rating).classList.add('active');
    }, function (err) {
        handleYoutubeAPIErrorResponse(err);
    });
}

async function ratePlaylist(rating) {
    if (!isValidRating(rating)) {
        return;
    }

    const playlistUrl = document.getElementById('playlist-url-input');
    const urlParams = new URLSearchParams(playlistUrl.value);
    const playlistId = urlParams.get('https://www.youtube.com/playlist?list');

    if (playlistId.length === 0) {
        return;
    }

    let progressBarElement = document.getElementById('rate-playlist-videos-progress-bar');
    progressBarElement.parentElement.style.display = 'flex';

    let playlistData = await getPlaylistData(playlistId, 50, !shouldUseCacheForRateVideos());

    for (let i = 0; i < playlistData.length; i++) {
        let percentage = (i / playlistData.length) * 100;
        progressBarElement.setAttribute('aria-valuenow', percentage.toString());
        progressBarElement.style.width = percentage + '%';

        let videoId = playlistData[i].snippet.resourceId.videoId;
        if (localStorage.getItem(videoId) === 'rated') {
            continue;
        }

        await rateVideoById(videoId, rating);
    }

    progressBarElement.parentElement.style.display = 'none';
}

function isValidRating(rating) {
    return rating === 'like' || rating === 'dislike' || rating === 'none';
}

function shouldUseCacheForSubscriptionsListRetrieval() {
    return isCheckboxChecked('get-subs-cache');
}

function shouldUseCacheForRateVideos() {
    return isCheckboxChecked('rate-playlist-videos-cache');
}

function isCheckboxChecked(id) {
    return document.getElementById(id).checked === true;
}

function handleYoutubeAPIErrorResponse(err) {
    console.error("Execute error", err);
    const errorBody = JSON.parse(err.body);
    if (errorBody && errorBody.error && errorBody.error.errors[0].reason === "quotaExceeded") {
        createToastNotification('Failed', 'Quota exceeded. Stopping the rating process. If you used cache, the rating progress has been saved in local storage. You can resume tomorrow, after the quota limit has been reset.', 30000);
        throw new Error('Quota exceeded');
    } else {
        createToastNotification('Failed', 'An error occurred during the last Youtube API call. Please check console logs.');
    }
}
